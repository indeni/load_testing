package json

import server.metrics.{ComplexMetric, DoubleMetric, Metric, MetricMessage}
import spray.json._

/**
  * Created by alex on 20/10/2016.
  */
trait JsonProtocols extends DefaultJsonProtocol {
  implicit object MetricFormat extends RootJsonFormat[Metric] {
    override def write(metric: Metric): JsValue = metric match {
      case doubleMetric: DoubleMetric =>
        JsObject("type" -> JsString("DoubleMetric"),
          "tags" -> doubleMetric.tags.toJson,
          "value" -> JsNumber(doubleMetric.value),
          "timestamp" -> JsNumber(doubleMetric.timeStamp))
      case jsonMetric: ComplexMetric =>
        JsObject("type" -> JsString("ComplexMetric"),
          "tags" -> jsonMetric.tags.toJson,
          "value" -> jsonMetric.value,
          "timestamp" -> JsNumber(jsonMetric.timeStamp))
    }

    override def read(json: JsValue): Metric = {
      json.asJsObject.fields("type") match {
        case JsString("DoubleMetric") =>
          json.asJsObject.getFields("tags", "value", "timestamp") match {
            case Seq(tags, JsNumber(value), JsNumber(timeStamp)) =>
              DoubleMetric(tags.convertTo[Map[String, String]], value.toDouble, timeStamp.toLong)
          }
        case JsString("ComplexMetric") =>
          json.asJsObject.getFields("tags", "value", "timestamp") match {
            case Seq(tags, value, JsNumber(timeStamp)) =>
              ComplexMetric(tags.convertTo[Map[String, String]], value, timeStamp.toLong)
          }
      }
    }
  }

  implicit object MetricMessageFormat extends RootJsonFormat[MetricMessage] {

    override def write(obj: MetricMessage): JsValue =
      JsObject("metrics" -> JsArray(obj.metrics.map(_.toJson).toVector), "tags" -> obj.tags.toJson)

    override def read(json: JsValue): MetricMessage = json.asJsObject.getFields("tags", "metrics") match {
      case Seq(tags, JsArray(metrics)) =>
        MetricMessage((for (metric <- metrics) yield metric.convertTo[Metric]).toList,
          tags.convertTo[Map[String, String]])
    }
  }

}
