package server.metrics

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class AlexSimulation extends Simulation {

  val scn = scenario("Scenario Name") // A scenario is a chain of requests and pauses
    .exec(http("request_1")
    .get("/"))
    .exec(http("request_2")
    .get("/computers?f=macbook"))
    .exec(http("request_3")
      .get("/computers/6"))
    .exec(http("request_4")
      .get("/"))
    .exec(http("request_5")
      .get("/computers?p=1"))
    .exec(http("request_6")
      .get("/computers?p=2"))
    .exec(http("request_7")
      .get("/computers?p=3"))
    .exec(http("request_8")
      .get("/computers?p=4"))
    .exec(http("request_9")
      .get("/computers/new"))
    .exec(http("request_10") // Here's an example of a POST request
      .post("/computers")
      .formParam("""name""", """Beautiful Computer""") // Note the triple double quotes: used in Scala for protecting a whole chain of characters (no need for backslash)
      .formParam("""introduced""", """2012-05-30""")
      .formParam("""discontinued""", """""")
      .formParam("""company""", """37"""))

  val httpConf = http
    .baseURL("http://computer-database.gatling.io") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:48.0) Gecko/20100101 Firefox/48.0")

  setUp(scn.inject(atOnceUsers(1)).protocols(httpConf))
}
