package server.metrics

/**
  * Created by alex on 18/10/2016.
  */

import spray.json._

trait Metric {
  type MetricType

  def tags: Map[String, String]

  def value: MetricType

  def timeStamp: Long

  def withTags(additionalTags: Map[String, String]): Metric

  /**
    * @param optionalTags The given optional tags.
    * @return A copy of this metric with the given optional tags; optional means that they will not override existing
    *         tags.
    */
  def withOptionalTags(optionalTags: Map[String, String]): Metric

  def name(): String = tags(Metric.MetricNameTag)
}

object Metric {
  val MetricNameTag = "im.name"
  val MetricTypeTag = "im.metric-type"
  val TsDataTypeTag = "im.dstype"
  val TsStepTag = "im.step"

  val TimestampTag = "timestamp"
  val ValueTag = "value"
}

case class DoubleMetric(tags: Map[String, String], value: Double, timeStamp: Long)
  extends Metric {
  override type MetricType = Double

  override def withTags(additionalTags: Map[String, String]): DoubleMetric =
    DoubleMetric(tags ++ additionalTags, value, timeStamp)

  override def withOptionalTags(optionalTags: Map[String, String]): DoubleMetric =
    DoubleMetric(optionalTags ++ tags, value, timeStamp)
}

case class ComplexMetric(tags: Map[String, String], value: JsValue, timeStamp: Long)
  extends Metric {
  override type MetricType = JsValue

  override def withTags(additionalTags: Map[String, String]): ComplexMetric =
    ComplexMetric(tags ++ additionalTags, value, timeStamp)

  override def withOptionalTags(optionalTags: Map[String, String]): ComplexMetric =
    ComplexMetric(optionalTags ++ tags, value, timeStamp)
}

case class MetricMessage(metrics: List[Metric], tags: Map[String, String] = Map())
