package server.metrics

import json.JsonProtocols
import spray.json._

/**
  * Created by alex on 10/10/2016.
  */
object MetricsGenerator extends App with JsonProtocols {
  val actTime = System.currentTimeMillis / 1000
  val value = scala.util.Random
  val offset = value.nextInt(100)
  val effectiveTime = actTime.-(offset)
  val tags = Map("test" -> offset.toString)
  private val metric: Metric = DoubleMetric(tags, offset, effectiveTime)
  val xxx = metric.toJson
  println(xxx)
}
