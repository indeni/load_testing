package server.metrics

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import json.JsonProtocols
import spray.json._

class RecordedSimulation extends Simulation with JsonProtocols{

	def generateBody(): String ={
		   	val actTime = System.currentTimeMillis / 1000
		   	val value = scala.util.Random
		   	val offset = value.nextInt(100)
		   	val effectiveTime = actTime.-(offset)
		   	val tags = Map("test" -> offset.toString)
		   	val metric: Metric = DoubleMetric(tags, offset, effectiveTime)
		   	MetricMessage(List(metric)).toJson.compactPrint
	}

	val scn = scenario("Register Device")
		.exec(http("Register Device")
			.post("/collector/hello")
//			.body(RawFileBody("RecordedSimulation_0000_request.txt"))
		)
	val sendMetrics = scenario("Send Metrics")
		.exec(http("Send Metrics")
			.post("/api/v1/collector/alex-collector/metrics")
//		  .body(StringBody(generateBody())).asJSON
		  .body(StringBody(session => s"""${generateBody}""")).asJSON
		)

	val httpConf = http
		.baseURL("https://192.168.1.203:9009") // Here is the root for all relative URLs
//		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
//		.acceptEncodingHeader("gzip, deflate")
//		.acceptLanguageHeader("en-US,en;q=0.5")
		.contentTypeHeader("application/json")
//		.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

	setUp(sendMetrics.inject(rampUsers(100) over (60 seconds)))
		.protocols(httpConf)
	  .assertions()
}